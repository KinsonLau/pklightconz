<?php
class ContactForm extends Form {
	
	public function __construct($controller, $name) {
        $fields = new FieldList(
            TextField::create("Name")->addExtraClass('required'),
            EmailField::create("Email")->addExtraClass('required'),
            TextareaField::create("Message")
        );
        $actions = new FieldList(FormAction::create("submit")->setTitle("Send"));

        $validation = new RequiredFields(array('Name', 'Email'));
         
        parent::__construct($controller, $name, $fields, $actions, $validation);
    }

    public function submit($data, $form) { 

		// check stopforumspam.com database
		$spam = false;
		$ip = $_SERVER['REMOTE_ADDR'];
		$res = file_get_contents('http://www.stopforumspam.com/api?f=json&ip='.$ip);
		if($res){
			$res = json_decode($res);
			if($res->success && $res->ip->appears){
				$spam = true;
			}
		}

		if($spam){
			return array(
	            'Content' => '<p>Sorry, an error occurred.</p>',
	            'Form' => ''
	        );
		}

		$config = SiteConfig::current_site_config();

        $email = new Email(); 
          
        $email->setTo($config->ContactEmail); 
        $email->setFrom($config->ContactEmail); 
        $email->replyTo($data['Email']);
        $email->setSubject("Contact Message from {$data["Name"]}"); 
          
        $messageBody = " 
            <p><strong>Name:</strong> {$data['Message']}</p>
            <p><strong>Email:</strong> {$data['Email']}</p>
            <p><strong>Message:</strong> {$data['Message']}</p> 
        "; 
        $email->setBody($messageBody); 
        $email->send();

        $form->resetValidation();

        return array(
            'Content' => '<p>Thank you for your email. We will be in touch as soon as possible.</p>',
            'Form' => ''
        );
    }

	public function forTemplate() {
        return $this->renderWith(array($this->class, 'ContactForm'));
    }
}