<?php
class AboutPage extends SiteTree {

	public static $db = array(
		'Blurb' => 'HTMLText'
	);

	public static $has_one = array(
		'ProfileImg' => 'Image'
	);
	
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		 $fields->addFieldsToTab('Root.Main', array(
		 	new UploadField('ProfileImg', 'Profile Picture'),
		 	new TextField('Blurb', 'Blurb')
        ), 'Content');
		
		return $fields;
	}

}

class AboutPage_Controller extends ContentController {


	public static $allowed_actions = array (
	);


}