<?php
class GalleryHolder extends Page {
    static $allowed_children = array('GalleryPage');
	public static $has_one = array(
		'Image' => 'Image'
	);
	public function getCMSFields() {
		$fields = parent::getCMSFields();
		 $fields->addFieldsToTab('Root.Main', array(
		),'');
		$fields->removeFieldFromTab("Root.Main","Metadata");
		$fields->removeFieldFromTab("Root.Main","Content");
		return $fields;
	}
	
}
class GalleryHolder_Controller extends GalleryPage_Controller {
	
	function GalleryProfile() { 
		$Image = DataObject::get_one("GalleryHolder"); 
		return ($Image) ? DataObject::get("GalleryPage", "ParentID = $Image->ID", "", $num) : false; 
	}
	
}

