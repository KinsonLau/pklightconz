<?php
class GalleryPage extends Page { 
	static $db = array(
        'Name' => 'Text'
    );
	public static $has_one = array(
		'GalleryImage' => 'Image'
	);
	public function getCMSFields() {
        $fields = parent::getCMSFields();
         
        $fields->addFieldsToTab('Root.Main', array(
        	new TextField('Name')	,
        	new UploadField('GalleryImage', 'Image')
		),'');
        $fields->removeFieldFromTab("Root.Main","Content");
		$fields->removeFieldFromTab("Root.Main","Metadata");
        return $fields;
    }
}

class GalleryPage_Controller extends Page_Controller {

}

