<?php
 
class CustomSiteConfig extends DataExtension {
     
    static $db = array(
        'Email' => 'Text',
        'Mobile' => 'Text',
        'Facebook' => 'Text',
        'Instagram' => 'Text'
    );
	public static $has_one = array(
	);
 
    public function updateCMSFields(FieldList $fields) {
        $fields->addFieldsToTab("Root.Main", array(
        		new TextField("Email", "Email"),
        		new TextField("Mobile", "Mobile"),
        		new TextField("Facebook", "Facebook"),
        		new TextField("Instagram", "Instagram")
		));
    }
}