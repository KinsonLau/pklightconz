<?php
class ContactPage extends Page{

	
}
class ContactPage_Controller extends Page_Controller{
	
	static $allowed_actions = array('Form');
    public function ContactForm() { 
        $fields = new FieldList( 
            new TextField('Name'), 
            new EmailField('Email'), 
            new TextareaField('Message')
        ); 
        $actions = new FieldList( 
            new FormAction('submit', 'Submit') 
        ); 
        return new Form($this, 'ContactForm', $fields, $actions); 
    }
	
	
	
}