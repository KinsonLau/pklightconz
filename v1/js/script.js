/* Author: Livia Lau
	
*/

$(document).ready(function(){

var slides = $('#slides');
var intro = $('#intro');
var bttn1 = $('.bttn');
var backstretch = $('.backstretch');

	$("#slides").click(function(e){
		// alert('clicked');

	});



// ========== Scrolling Down ========
// ==================================



	//Scrolling 
	var duration = 1000;
	var $navItems = $("header nav a");
	
	$navItems.click(function(e){
		e.preventDefault();
		var id = $(this).attr("href");
		var scrollEnd = $(id).offset().top - 130;	
		$("html, body").stop(true,true).animate({scrollTop: scrollEnd}, duration, "easeOutExpo");
	});

// ==========================



// ========== Form validator ======

$( ".myform" ).validate();



////////Form Validation	
	
	//Form variables
	
		var $name = $("#name"),
		$email = $("#email"),
		$message = $("#message"),
		$spam = $("#spam"),
		$formFields = $("input:text, textarea"),
		$status = $("#status"),
		$contactForm = $(".contact-form");
		
		//initialise
		$status.hide();	
		
		
			/*submit handler for contact form*/
		$contactForm.submit(function(e) {	
				
				//prevent default form submit action
			 	e.preventDefault();
				
				//clear all error borders from form fields
				$formFields.removeClass("error-focus");
				
				//check required fields are not empty and that the email address is valid
				if($name.val()==""){
					
						setStatusMessage("Please Enter Your Name");
						$name.setFocus();
					
				}else if($email.val()==""){
					
						setStatusMessage("Please Enter Your Email Address");
						$email.setFocus();
					
				}else if(!isValidEmail($email.val())){
					
						setStatusMessage("Please Enter a Correct Email Address");
						$email.setFocus();
					
				
					
				}else if($message.val()==""){
					
						setStatusMessage("Please Enter Your Message");
						$message.setFocus();
					
				}else if(!$spam.val()==""){
					
						setStatusMessage("Spam");
	
				}else{
					
						//if all fields are valid then send data to the server for processing and didplay "please wait" message
						setStatusMessage("Email being sent... please wait");
					
						//serialize all the form field values as a string
						var formData = $(this).serialize();
					
					//send serialized data string to the send mail php via POST method
					
					$.post("send_mail.php", function(sent){
						
						if(sent=="error"){ 
							 
								 	setStatusMessage("Opps, something went wrong - message not sent");
								 
							  } else if(sent=="success"){
									
									setStatusMessage("Thanks "+$name.val()+", A confirmation email will be sent to you shortly");
									
									//clear form fields
									$formFields.val("");
									
							  }//end if else
						 
						},"html");
					
				}//end else
	
	   });//end submit


//placeholder fix

(function($) {
  $.fn.placeholder = function() {
    if(typeof document.createElement("input").placeholder == 'undefined') {
      $('[placeholder]').focus(function() {
        var input = $(this);
        if (input.val() == input.attr('placeholder')) {
          input.val('');
          input.removeClass('placeholder');
        }
      }).blur(function() {
        var input = $(this);
        if (input.val() == '' || input.val() == input.attr('placeholder')) {
          input.addClass('placeholder');
          input.val(input.attr('placeholder'));
        }
      }).blur().parents('form').submit(function() {
        $(this).find('[placeholder]').each(function() {
          var input = $(this);
          if (input.val() == input.attr('placeholder')) {
            input.val('');
          }
      })
    });
  }
}
})(jQuery);




});




