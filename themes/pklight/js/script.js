/* Author: Livia Lau
	
*/

$(document).ready(function(){
$('#slider').nivoSlider({effect:'slideInRight'});

var slides = $('#slides');
var intro = $('#intro');
var bttn1 = $('.bttn');
var backstretch = $('.backstretch');

	$("#slides").click(function(e){
		// alert('clicked');

	});



// ========== Scrolling Down ========
// ==================================



	//Scrolling 
	var duration = 1000;
	var $navItems = $("header nav a");
	
	$navItems.click(function(e){
		e.preventDefault();
		var id = $(this).attr("data-url");
		//alert('#'+id);
		var scrollEnd = $('#'+id).offset().top - 130;	
		
		$("html, body").stop(true,true).animate({scrollTop: scrollEnd}, duration, "easeOutExpo");
	});

// ==========================



// ========== Form validator ======

$( "form" ).validate();



////////Form Validation	
	
	//Form variables
	
		var $name = $("#name"),
		$email = $("#email"),
		$message = $("#message"),
		$spam = $("#spam"),
		$formFields = $("input:text, textarea"),
		$status = $("#status"),
		$contactForm = $(".contact-form");
		
		//initialise
		$status.hide();	
		
		
		


//placeholder fix

(function($) {
  $.fn.placeholder = function() {
    if(typeof document.createElement("input").placeholder == 'undefined') {
      $('[placeholder]').focus(function() {
        var input = $(this);
        if (input.val() == input.attr('placeholder')) {
          input.val('');
          input.removeClass('placeholder');
        }
      }).blur(function() {
        var input = $(this);
        if (input.val() == '' || input.val() == input.attr('placeholder')) {
          input.addClass('placeholder');
          input.val(input.attr('placeholder'));
        }
      }).blur().parents('form').submit(function() {
        $(this).find('[placeholder]').each(function() {
          var input = $(this);
          if (input.val() == input.attr('placeholder')) {
            input.val('');
          }
      })
    });
  }
}
})(jQuery);




});




