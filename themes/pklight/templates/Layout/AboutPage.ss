<section id="about" class="page">
	<div class="about-wrapper bg clearfix">
            <div class="left-col text_center">
            $Title
            $ProfileImg
              <img class="profile" src="$ProfileImg.Url" alt="Profile">
              <h3>'Simple and Effective'</h3>
            </div>
            <div class="right-col">
                <p>
                <span><em>I'm always searching for the 'a-ha' moment when the balance is struck between the formal components of the object and the desired charisma and impact"</em></span>
                </p>
                <p>
                Based in Auckland, New Zealand, light-designer and object-maker Phillippa Kenny has been creating light works for the past 9 years
                </p>
                <p>
                Phillippa's lights are suited to commercial as well as domestic spaces, providing the local interest of an object d'art while also adding atmosphere through colour and tone. In designing her current serires of lights, Phillippa continues to gravitate toward the simple beauty of line &amp; pattern while incorporating more recent explorations into the interplay of texture and colour.
                </p>
                <p>Several of Phillippa's artworks are included in the Wallace Arts Trust Collection, and she has exhibited in a number of Auckland galleries and outdoor site-specific exhibitions.</p>

                <!-- awards -->
                <div class="info">
                  <h4>Awards:</h4>
                  <div class="info-left">
                    <p>First place - Untitled 2001</p>
                  </div>
                  <div class="info-right">
                    <p>Depot Art Award, Depot Art &amp; Music Space, Devonport, Auckland</p>
                  </div>
                </div><!-- end info -->

                <div class="info">
                  <h4>Academic Qualifications:</h4>
                  <div class="info-left">
                    <p>Bachelor of Visual Arts 2001</p>
                  </div>
                  <div class="info-right">
                    <p>Auckland University of Technology</p>
                  </div>
                </div><!-- end info -->
            </div><!-- right-col -->
          </div><!--end about wrapper -->
</section>