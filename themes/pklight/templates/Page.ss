<!doctype Html>
	<!--[if lt IE 7]> <html class="no-js ie7-below oldie" lang="en"> <![endif]-->
	<!--[if IE 7]>    <html class="no-js ie7 oldie" lang="en"> <![endif]-->
	<!--[if IE 8]>    <html class="no-js ie8 oldie" lang="en"> <![endif]-->
	<!--[if IE 9]>    <html class="no-js ie9 oldie" lang="en"> <![endif]-->
	<!--[if gt IE 8]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
	<head>
		<% base_tag %>
		<meta charset="utf-8">
		<title>Phillippa Kenny</title>
		<meta name="description" content="">
		<meta name="keywords" content="">
		<link rel="stylesheet" href="$ThemeDir/css/font-awesome.min.css">
   		<link rel="stylesheet" href="$ThemeDir/themes/default/default.css" type="text/css" media="screen" />

		<!--[if IE 7]>
  		<link rel="stylesheet" href="$ThemeDir/css/font-awesome-ie7.css">
  		<![endif]-->
		<link rel="stylesheet" href="$ThemeDir/css/style.css">
		<link rel="stylesheet" href="$ThemeDir/css/nivo-slider.css">
    	<link rel="stylesheet" href="$ThemeDir/css/lightbox.css">

 		<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js"></script>
		
		<link rel="shortcut icon" href="img/favicon.gif">
		<script src="$ThemeDir/js/libs/modernizr-2.5.3.min.js"></script>
	</head>
<body>
		<% include Header %>
		<% include SideBar %>
		<div class="wrapper">
		
			$Layout
			
        </div>
        <footer>
        </footer>  
    	</div><!--end .wrapper-->
		
		<!-- javascript -->

		<script src="$ThemeDir/js/libs/jquery.min.js"></script>
		<script type="text/javascript">window.jQuery || document.write('<script src="$ThemeDir/js/libs/jquery-1.10.2.min.js"><\/script>')</script>
    	<script src="$ThemeDir/js/libs/jquery.nivo.slider.js"></script>
    	<script src="$ThemeDir/js/libs/lightbox-2.6.min.js"></script>
		<script src="$ThemeDir/js/libs/jquery.easing.1.3.js"></script>
    	<script src="$ThemeDir/js/libs/jquery.validate.js"></script>
		<script src="$ThemeDir/js/script.js"></script>
		
	</body>
</html>