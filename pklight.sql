-- phpMyAdmin SQL Dump
-- version 3.5.7
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Sep 09, 2013 at 10:33 AM
-- Server version: 5.5.29
-- PHP Version: 5.4.10

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `pklight`
--

-- --------------------------------------------------------

--
-- Table structure for table `AboutPage`
--

CREATE TABLE `AboutPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ProfileImgID` int(11) NOT NULL DEFAULT '0',
  `Blurb` mediumtext,
  `GalleryImgID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ProfileImgID` (`ProfileImgID`),
  KEY `GalleryImgID` (`GalleryImgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `AboutPage`
--

INSERT INTO `AboutPage` (`ID`, `ProfileImgID`, `Blurb`, `GalleryImgID`) VALUES
(2, 4, 'Simple and Effective', 0);

-- --------------------------------------------------------

--
-- Table structure for table `AboutPage_Live`
--

CREATE TABLE `AboutPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ProfileImgID` int(11) NOT NULL DEFAULT '0',
  `Blurb` mediumtext,
  `GalleryImgID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ProfileImgID` (`ProfileImgID`),
  KEY `GalleryImgID` (`GalleryImgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `AboutPage_Live`
--

INSERT INTO `AboutPage_Live` (`ID`, `ProfileImgID`, `Blurb`, `GalleryImgID`) VALUES
(2, 4, 'Simple and Effective', 0);

-- --------------------------------------------------------

--
-- Table structure for table `AboutPage_versions`
--

CREATE TABLE `AboutPage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ProfileImgID` int(11) NOT NULL DEFAULT '0',
  `Blurb` mediumtext,
  `GalleryImgID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `ProfileImgID` (`ProfileImgID`),
  KEY `GalleryImgID` (`GalleryImgID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=14 ;

--
-- Dumping data for table `AboutPage_versions`
--

INSERT INTO `AboutPage_versions` (`ID`, `RecordID`, `Version`, `ProfileImgID`, `Blurb`, `GalleryImgID`) VALUES
(1, 2, 3, 0, NULL, 0),
(2, 2, 4, 4, NULL, 0),
(3, 2, 5, 4, 'Simple and Effective', 0),
(4, 2, 6, 4, 'Simple and Effective', 0),
(5, 2, 7, 4, 'Simple and Effective', 0),
(6, 2, 8, 4, 'Simple and Effective', 0),
(7, 2, 9, 4, 'Simple and Effective', 0),
(8, 2, 10, 4, 'Simple and Effective', 0),
(9, 2, 11, 4, 'Simple and Effective', 0),
(10, 2, 12, 4, 'Simple and Effective', 0),
(11, 2, 13, 4, 'Simple and Effective', 0),
(12, 2, 14, 4, 'Simple and Effective', 0),
(13, 2, 15, 4, 'Simple and Effective', 0);

-- --------------------------------------------------------

--
-- Table structure for table `EditableFormField`
--

CREATE TABLE `EditableFormField` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('EditableFormField','EditableCheckbox','EditableCountryDropdownField','EditableDateField','EditableEmailField','EditableFileField','EditableFormHeading','EditableLiteralField','EditableMemberListField','EditableMultipleOptionField','EditableCheckboxGroupField','EditableDropdown','EditableRadioField','EditableTextField') DEFAULT 'EditableFormField',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Default` varchar(50) DEFAULT NULL,
  `Sort` int(11) NOT NULL DEFAULT '0',
  `Required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CustomErrorMessage` varchar(255) DEFAULT NULL,
  `CustomRules` mediumtext,
  `CustomSettings` mediumtext,
  `CustomParameter` varchar(200) DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `EditableFormField_Live`
--

CREATE TABLE `EditableFormField_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('EditableFormField','EditableCheckbox','EditableCountryDropdownField','EditableDateField','EditableEmailField','EditableFileField','EditableFormHeading','EditableLiteralField','EditableMemberListField','EditableMultipleOptionField','EditableCheckboxGroupField','EditableDropdown','EditableRadioField','EditableTextField') DEFAULT 'EditableFormField',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Default` varchar(50) DEFAULT NULL,
  `Sort` int(11) NOT NULL DEFAULT '0',
  `Required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CustomErrorMessage` varchar(255) DEFAULT NULL,
  `CustomRules` mediumtext,
  `CustomSettings` mediumtext,
  `CustomParameter` varchar(200) DEFAULT NULL,
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `EditableFormField_versions`
--

CREATE TABLE `EditableFormField_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('EditableFormField','EditableCheckbox','EditableCountryDropdownField','EditableDateField','EditableEmailField','EditableFileField','EditableFormHeading','EditableLiteralField','EditableMemberListField','EditableMultipleOptionField','EditableCheckboxGroupField','EditableDropdown','EditableRadioField','EditableTextField') DEFAULT 'EditableFormField',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Default` varchar(50) DEFAULT NULL,
  `Sort` int(11) NOT NULL DEFAULT '0',
  `Required` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `CustomErrorMessage` varchar(255) DEFAULT NULL,
  `CustomRules` mediumtext,
  `CustomSettings` mediumtext,
  `CustomParameter` varchar(200) DEFAULT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `ParentID` (`ParentID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `EditableOption`
--

CREATE TABLE `EditableOption` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('EditableOption') DEFAULT 'EditableOption',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `EditableOption_Live`
--

CREATE TABLE `EditableOption_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('EditableOption') DEFAULT 'EditableOption',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `EditableOption_versions`
--

CREATE TABLE `EditableOption_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('EditableOption') DEFAULT 'EditableOption',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Default` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `ParentID` (`ParentID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Email_BounceRecord`
--

CREATE TABLE `Email_BounceRecord` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Email_BounceRecord') DEFAULT 'Email_BounceRecord',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `BounceEmail` varchar(50) DEFAULT NULL,
  `BounceTime` datetime DEFAULT NULL,
  `BounceMessage` varchar(50) DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `MemberID` (`MemberID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `ErrorPage`
--

CREATE TABLE `ErrorPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ErrorCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `ErrorPage`
--

INSERT INTO `ErrorPage` (`ID`, `ErrorCode`) VALUES
(4, 404),
(5, 500);

-- --------------------------------------------------------

--
-- Table structure for table `ErrorPage_Live`
--

CREATE TABLE `ErrorPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ErrorCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `ErrorPage_Live`
--

INSERT INTO `ErrorPage_Live` (`ID`, `ErrorCode`) VALUES
(4, 404),
(5, 500);

-- --------------------------------------------------------

--
-- Table structure for table `ErrorPage_versions`
--

CREATE TABLE `ErrorPage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ErrorCode` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `File`
--

CREATE TABLE `File` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('File','Folder','Image','Image_Cached') DEFAULT 'File',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Name` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Filename` mediumtext,
  `Content` mediumtext,
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  `OwnerID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `OwnerID` (`OwnerID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=9 ;

--
-- Dumping data for table `File`
--

INSERT INTO `File` (`ID`, `ClassName`, `Created`, `LastEdited`, `Name`, `Title`, `Filename`, `Content`, `ShowInSearch`, `ParentID`, `OwnerID`) VALUES
(1, 'Folder', '2013-09-08 11:02:18', '2013-09-08 11:02:18', 'Uploads', 'Uploads', 'assets/Uploads/', NULL, 1, 0, 0),
(2, 'File', '2013-09-08 11:02:18', '2013-09-08 11:02:18', 'error-404.html', 'error-404.html', 'assets/error-404.html', NULL, 1, 0, 0),
(3, 'File', '2013-09-08 11:02:18', '2013-09-08 11:02:18', 'error-500.html', 'error-500.html', 'assets/error-500.html', NULL, 1, 0, 0),
(4, 'Image', '2013-09-08 01:57:37', '2013-09-08 01:57:37', 'profile.jpg', 'profile', 'assets/Uploads/profile.jpg', NULL, 1, 1, 1),
(5, 'Image', '2013-09-08 04:54:39', '2013-09-08 04:54:39', 'image-1.jpg', 'image 1', 'assets/Uploads/image-1.jpg', NULL, 1, 1, 1),
(6, 'Image', '2013-09-08 05:10:15', '2013-09-08 05:10:15', 'image-2.jpg', 'image 2', 'assets/Uploads/image-2.jpg', NULL, 1, 1, 1),
(7, 'Image', '2013-09-08 06:23:25', '2013-09-08 06:23:25', '1.jpg', '1', 'assets/Uploads/1.jpg', NULL, 1, 1, 1),
(8, 'Image', '2013-09-08 08:16:15', '2013-09-08 08:16:15', '3.jpg', '3', 'assets/Uploads/3.jpg', NULL, 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `GalleryHolder`
--

CREATE TABLE `GalleryHolder` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ImageID` (`ImageID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `GalleryHolder`
--

INSERT INTO `GalleryHolder` (`ID`, `ImageID`) VALUES
(11, 0),
(14, 0);

-- --------------------------------------------------------

--
-- Table structure for table `GalleryHolder_Live`
--

CREATE TABLE `GalleryHolder_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ImageID` (`ImageID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=15 ;

--
-- Dumping data for table `GalleryHolder_Live`
--

INSERT INTO `GalleryHolder_Live` (`ID`, `ImageID`) VALUES
(9, 0),
(11, 0),
(14, 0);

-- --------------------------------------------------------

--
-- Table structure for table `GalleryHolder_versions`
--

CREATE TABLE `GalleryHolder_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `ImageID` (`ImageID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=12 ;

--
-- Dumping data for table `GalleryHolder_versions`
--

INSERT INTO `GalleryHolder_versions` (`ID`, `RecordID`, `Version`, `ImageID`) VALUES
(6, 11, 4, 0),
(7, 14, 1, 0),
(8, 14, 2, 0),
(9, 14, 3, 0),
(10, 14, 4, 0),
(11, 14, 5, 0);

-- --------------------------------------------------------

--
-- Table structure for table `GalleryPage`
--

CREATE TABLE `GalleryPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Blurb` mediumtext,
  `GalleryImgID` int(11) NOT NULL DEFAULT '0',
  `Name` mediumtext,
  `Description` mediumtext,
  `ImageID` int(11) NOT NULL DEFAULT '0',
  `GalleryImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GalleryImgID` (`GalleryImgID`),
  KEY `ImageID` (`ImageID`),
  KEY `GalleryImageID` (`GalleryImageID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `GalleryPage`
--

INSERT INTO `GalleryPage` (`ID`, `Blurb`, `GalleryImgID`, `Name`, `Description`, `ImageID`, `GalleryImageID`) VALUES
(7, NULL, 0, NULL, NULL, 0, 0),
(8, NULL, 0, NULL, NULL, 0, 0),
(12, NULL, 0, 'Image One Title', NULL, 5, 5),
(13, NULL, 0, 'Image Two Title', NULL, 0, 6),
(15, NULL, 0, 'This is an example of a caption', NULL, 0, 7),
(17, NULL, 0, 'Caption Two', NULL, 0, 8);

-- --------------------------------------------------------

--
-- Table structure for table `GalleryPage_Live`
--

CREATE TABLE `GalleryPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Blurb` mediumtext,
  `GalleryImgID` int(11) NOT NULL DEFAULT '0',
  `Name` mediumtext,
  `Description` mediumtext,
  `ImageID` int(11) NOT NULL DEFAULT '0',
  `GalleryImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GalleryImgID` (`GalleryImgID`),
  KEY `ImageID` (`ImageID`),
  KEY `GalleryImageID` (`GalleryImageID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `GalleryPage_Live`
--

INSERT INTO `GalleryPage_Live` (`ID`, `Blurb`, `GalleryImgID`, `Name`, `Description`, `ImageID`, `GalleryImageID`) VALUES
(9, NULL, 0, NULL, NULL, 0, 0),
(12, NULL, 0, 'Image One Title', NULL, 5, 5),
(13, NULL, 0, 'Image Two Title', NULL, 0, 6),
(15, NULL, 0, 'This is an example of a caption', NULL, 0, 7),
(17, NULL, 0, 'Caption Two', NULL, 0, 8);

-- --------------------------------------------------------

--
-- Table structure for table `GalleryPage_versions`
--

CREATE TABLE `GalleryPage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Blurb` mediumtext,
  `GalleryImgID` int(11) NOT NULL DEFAULT '0',
  `Name` mediumtext,
  `Description` mediumtext,
  `ImageID` int(11) NOT NULL DEFAULT '0',
  `GalleryImageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `GalleryImgID` (`GalleryImgID`),
  KEY `ImageID` (`ImageID`),
  KEY `GalleryImageID` (`GalleryImageID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `GalleryPage_versions`
--

INSERT INTO `GalleryPage_versions` (`ID`, `RecordID`, `Version`, `Blurb`, `GalleryImgID`, `Name`, `Description`, `ImageID`, `GalleryImageID`) VALUES
(13, 15, 1, NULL, 0, NULL, NULL, 0, 0),
(14, 15, 2, NULL, 0, NULL, NULL, 0, 7),
(15, 15, 3, NULL, 0, 'This is an example of a caption', NULL, 0, 7),
(16, 17, 1, NULL, 0, NULL, NULL, 0, 0),
(17, 17, 2, NULL, 0, NULL, NULL, 0, 8),
(18, 17, 3, NULL, 0, 'Caption Two', NULL, 0, 8),
(19, 17, 4, NULL, 0, 'Caption Two', NULL, 0, 8),
(20, 17, 5, NULL, 0, 'Caption Two', NULL, 0, 8);

-- --------------------------------------------------------

--
-- Table structure for table `Group`
--

CREATE TABLE `Group` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Group') DEFAULT 'Group',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `Description` mediumtext,
  `Code` varchar(50) DEFAULT NULL,
  `Locked` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HtmlEditorConfig` varchar(50) DEFAULT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `Group`
--

INSERT INTO `Group` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Description`, `Code`, `Locked`, `Sort`, `HtmlEditorConfig`, `ParentID`) VALUES
(1, 'Group', '2013-09-08 01:02:17', '2013-09-08 01:02:17', 'Content Authors', NULL, 'content-authors', 0, 1, NULL, 0),
(2, 'Group', '2013-09-08 01:02:17', '2013-09-08 01:02:17', 'Administrators', NULL, 'administrators', 0, 0, NULL, 0);

-- --------------------------------------------------------

--
-- Table structure for table `Group_Members`
--

CREATE TABLE `Group_Members` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GroupID` (`GroupID`),
  KEY `MemberID` (`MemberID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `Group_Members`
--

INSERT INTO `Group_Members` (`ID`, `GroupID`, `MemberID`) VALUES
(1, 2, 1),
(2, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `Group_Roles`
--

CREATE TABLE `Group_Roles` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GroupID` int(11) NOT NULL DEFAULT '0',
  `PermissionRoleID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GroupID` (`GroupID`),
  KEY `PermissionRoleID` (`PermissionRoleID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `LoginAttempt`
--

CREATE TABLE `LoginAttempt` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('LoginAttempt') DEFAULT 'LoginAttempt',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Email` varchar(255) DEFAULT NULL,
  `Status` enum('Success','Failure') DEFAULT 'Success',
  `IP` varchar(255) DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `MemberID` (`MemberID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `Member`
--

CREATE TABLE `Member` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Member') DEFAULT 'Member',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `FirstName` varchar(50) DEFAULT NULL,
  `Surname` varchar(50) DEFAULT NULL,
  `Email` varchar(256) DEFAULT NULL,
  `Password` varchar(160) DEFAULT NULL,
  `RememberLoginToken` varchar(160) DEFAULT NULL,
  `NumVisit` int(11) NOT NULL DEFAULT '0',
  `LastVisited` datetime DEFAULT NULL,
  `Bounced` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AutoLoginHash` varchar(160) DEFAULT NULL,
  `AutoLoginExpired` datetime DEFAULT NULL,
  `PasswordEncryption` varchar(50) DEFAULT NULL,
  `Salt` varchar(50) DEFAULT NULL,
  `PasswordExpiry` date DEFAULT NULL,
  `LockedOutUntil` datetime DEFAULT NULL,
  `Locale` varchar(6) DEFAULT NULL,
  `FailedLoginCount` int(11) NOT NULL DEFAULT '0',
  `DateFormat` varchar(30) DEFAULT NULL,
  `TimeFormat` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `Email` (`Email`(255)),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `Member`
--

INSERT INTO `Member` (`ID`, `ClassName`, `Created`, `LastEdited`, `FirstName`, `Surname`, `Email`, `Password`, `RememberLoginToken`, `NumVisit`, `LastVisited`, `Bounced`, `AutoLoginHash`, `AutoLoginExpired`, `PasswordEncryption`, `Salt`, `PasswordExpiry`, `LockedOutUntil`, `Locale`, `FailedLoginCount`, `DateFormat`, `TimeFormat`) VALUES
(1, 'Member', '2013-09-08 01:02:17', '2013-09-09 08:14:15', 'Default Admin', NULL, 'admin', '$2y$10$a9f9a9d70aa61c4c41fceuuIqKYZHi4Q.hj0Oca.Mr/tufprH5RhK', NULL, 4, '2013-09-09 20:29:54', 0, NULL, NULL, 'blowfish', '10$a9f9a9d70aa61c4c41fce7', NULL, NULL, 'en_US', 0, NULL, NULL),
(2, 'Member', '2013-09-08 07:05:44', '2013-09-08 07:05:44', 'Phillippa', 'Kenny', 'kinson_11@hotmail.com', '$2y$10$dd3d584388d97059c2e47ODT/PY7ru/Sx0feJ0K4Scm8/.kwlWxHu', NULL, 0, NULL, 0, NULL, NULL, 'blowfish', '10$dd3d584388d97059c2e47c', NULL, NULL, 'en_US', 0, 'MMM d, y', 'h:mm:ss a');

-- --------------------------------------------------------

--
-- Table structure for table `MemberPassword`
--

CREATE TABLE `MemberPassword` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('MemberPassword') DEFAULT 'MemberPassword',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Password` varchar(160) DEFAULT NULL,
  `Salt` varchar(50) DEFAULT NULL,
  `PasswordEncryption` varchar(50) DEFAULT NULL,
  `MemberID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `MemberID` (`MemberID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `MemberPassword`
--

INSERT INTO `MemberPassword` (`ID`, `ClassName`, `Created`, `LastEdited`, `Password`, `Salt`, `PasswordEncryption`, `MemberID`) VALUES
(1, 'MemberPassword', '2013-09-08 01:02:18', '2013-09-08 01:02:18', '$2y$10$a9f9a9d70aa61c4c41fceuuIqKYZHi4Q.hj0Oca.Mr/tufprH5RhK', '10$a9f9a9d70aa61c4c41fce7', 'blowfish', 1),
(2, 'MemberPassword', '2013-09-08 07:05:44', '2013-09-08 07:05:44', '$2y$10$dd3d584388d97059c2e47ODT/PY7ru/Sx0feJ0K4Scm8/.kwlWxHu', '10$dd3d584388d97059c2e47c', 'blowfish', 2);

-- --------------------------------------------------------

--
-- Table structure for table `Permission`
--

CREATE TABLE `Permission` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Permission') DEFAULT 'Permission',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Code` varchar(50) DEFAULT NULL,
  `Arg` int(11) NOT NULL DEFAULT '0',
  `Type` int(11) NOT NULL DEFAULT '1',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GroupID` (`GroupID`),
  KEY `Code` (`Code`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `Permission`
--

INSERT INTO `Permission` (`ID`, `ClassName`, `Created`, `LastEdited`, `Code`, `Arg`, `Type`, `GroupID`) VALUES
(1, 'Permission', '2013-09-08 01:02:17', '2013-09-08 01:02:17', 'CMS_ACCESS_CMSMain', 0, 1, 1),
(2, 'Permission', '2013-09-08 01:02:17', '2013-09-08 01:02:17', 'CMS_ACCESS_AssetAdmin', 0, 1, 1),
(3, 'Permission', '2013-09-08 01:02:17', '2013-09-08 01:02:17', 'CMS_ACCESS_ReportAdmin', 0, 1, 1),
(4, 'Permission', '2013-09-08 01:02:17', '2013-09-08 01:02:17', 'SITETREE_REORGANISE', 0, 1, 1),
(5, 'Permission', '2013-09-08 01:02:17', '2013-09-08 01:02:17', 'ADMIN', 0, 1, 2);

-- --------------------------------------------------------

--
-- Table structure for table `PermissionRole`
--

CREATE TABLE `PermissionRole` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('PermissionRole') DEFAULT 'PermissionRole',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(50) DEFAULT NULL,
  `OnlyAdminCanApply` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `PermissionRoleCode`
--

CREATE TABLE `PermissionRoleCode` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('PermissionRoleCode') DEFAULT 'PermissionRoleCode',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Code` varchar(50) DEFAULT NULL,
  `RoleID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RoleID` (`RoleID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `RedirectorPage`
--

CREATE TABLE `RedirectorPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RedirectionType` enum('Internal','External') DEFAULT 'Internal',
  `ExternalURL` varchar(2083) DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `LinkToID` (`LinkToID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `RedirectorPage_Live`
--

CREATE TABLE `RedirectorPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RedirectionType` enum('Internal','External') DEFAULT 'Internal',
  `ExternalURL` varchar(2083) DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `LinkToID` (`LinkToID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `RedirectorPage_versions`
--

CREATE TABLE `RedirectorPage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `RedirectionType` enum('Internal','External') DEFAULT 'Internal',
  `ExternalURL` varchar(2083) DEFAULT NULL,
  `LinkToID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `LinkToID` (`LinkToID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SiteConfig`
--

CREATE TABLE `SiteConfig` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SiteConfig') DEFAULT 'SiteConfig',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `Tagline` varchar(255) DEFAULT NULL,
  `Theme` varchar(255) DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers') DEFAULT 'Anyone',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers') DEFAULT 'LoggedInUsers',
  `CanCreateTopLevelType` enum('LoggedInUsers','OnlyTheseUsers') DEFAULT 'LoggedInUsers',
  `FooterContent` mediumtext,
  `Email` mediumtext,
  `Mobile` mediumtext,
  `Facebook` mediumtext,
  `Instagram` mediumtext,
  PRIMARY KEY (`ID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `SiteConfig`
--

INSERT INTO `SiteConfig` (`ID`, `ClassName`, `Created`, `LastEdited`, `Title`, `Tagline`, `Theme`, `CanViewType`, `CanEditType`, `CanCreateTopLevelType`, `FooterContent`, `Email`, `Mobile`, `Facebook`, `Instagram`) VALUES
(1, 'SiteConfig', '2013-09-08 01:02:17', '2013-09-08 08:26:36', 'Pk Light Design', 'Pk Design', 'pklight', 'Anyone', 'LoggedInUsers', 'LoggedInUsers', NULL, 'pk_art@hotmail.com', '+64 21 237 8259', 'http://www.facebook.com/', 'http://instagram.com/');

-- --------------------------------------------------------

--
-- Table structure for table `SiteConfig_CreateTopLevelGroups`
--

CREATE TABLE `SiteConfig_CreateTopLevelGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteConfigID` (`SiteConfigID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SiteConfig_EditorGroups`
--

CREATE TABLE `SiteConfig_EditorGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteConfigID` (`SiteConfigID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SiteConfig_ViewerGroups`
--

CREATE TABLE `SiteConfig_ViewerGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteConfigID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteConfigID` (`SiteConfigID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SiteTree`
--

CREATE TABLE `SiteTree` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Page','ErrorPage','AboutPage','GalleryHolder','GalleryPage','ContactPage','HomePage','SiteTree','RedirectorPage','VirtualPage') DEFAULT 'Page',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `URLSegment` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `MenuTitle` varchar(100) DEFAULT NULL,
  `Content` mediumtext,
  `MetaTitle` varchar(255) DEFAULT NULL,
  `MetaDescription` mediumtext,
  `MetaKeywords` varchar(1024) DEFAULT NULL,
  `ExtraMeta` mediumtext,
  `ShowInMenus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ReportClass` varchar(50) DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `URLSegment` (`URLSegment`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `SiteTree`
--

INSERT INTO `SiteTree` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaTitle`, `MetaDescription`, `MetaKeywords`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `Version`, `ParentID`) VALUES
(1, 'HomePage', '2013-09-08 01:02:17', '2013-09-09 09:36:11', 'home', 'Home', NULL, '<h4>Studio:</h4>\n<p>149 Queent St<br>Northcote Point<br>Auckland 6027<br>New Zealand</p>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 19, 0),
(2, 'AboutPage', '2013-09-08 01:02:17', '2013-09-08 07:04:05', 'about', 'About', NULL, '<p><em><span class="itl">"I''m always searching for the ''a-ha'' moment when the balance is struck between the formal components of the object and the desired charisma and impact"</span></em></p>\n<p>Based in Auckland, New Zealand, light-designer and object-maker Phillippa Kenny has been creating light works for the past 9 years</p>\n<p>Phillippa''s lights are suited to commercial as well as domestic spaces, providing the local interest of an object d''art while also adding atmosphere through colour and tone. In designing her current serires of lights, Phillippa continues to gravitate toward the simple beauty of line &amp; pattern while incorporating more recent explorations into the interplay of texture and colour.</p>\n<p>Several of Phillippa''s artworks are included in the Wallace Arts Trust Collection, and she has exhibited in a number of Auckland galleries and outdoor site-specific exhibitions.</p>\n<table style="width: 400px; margin: 25px 0px 10px;" border="0"><tbody><tr><td>\n<h4>Awards:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>First place - Untitled 2001</span></td>\n<td><span>Depot Art Award, Depot Art &amp; Music Space, Devonport, Auckland</span></td>\n</tr></tbody></table><table style="width: 400px;" border="0"><tbody><tr><td>\n<h4>Academic Qualifications:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>Bachelor of Visual Arts 2001</span></td>\n<td><span>Auckland University of Technology</span></td>\n</tr></tbody></table>', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 15, 0),
(4, 'ErrorPage', '2013-09-08 01:02:17', '2013-09-08 01:02:18', 'page-not-found', 'Page not found', NULL, '<p>Sorry, it seems you were trying to access a page that doesn''t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>', NULL, NULL, NULL, NULL, 0, 0, 7, 0, 0, NULL, 'Inherit', 'Inherit', 1, 0),
(5, 'ErrorPage', '2013-09-08 01:02:18', '2013-09-08 01:02:18', 'server-error', 'Server error', NULL, '<p>Sorry, there was a problem with handling your request.</p>', NULL, NULL, NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'Inherit', 1, 0),
(11, 'GalleryHolder', '2013-09-08 04:51:32', '2013-09-08 06:01:23', 'gallery', 'Gallery', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', 4, 0),
(12, 'GalleryPage', '2013-09-08 04:52:41', '2013-09-08 05:03:07', 'image-one', 'Image One', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 6, 11),
(13, 'GalleryPage', '2013-09-08 05:09:58', '2013-09-08 05:10:18', 'image-two', 'Image Two', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 3, 11),
(14, 'GalleryHolder', '2013-09-08 06:22:22', '2013-09-08 06:26:27', 'slideshow', 'Slideshow', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 5, 0),
(15, 'GalleryPage', '2013-09-08 06:22:45', '2013-09-08 06:23:48', 'slideshow-one', 'Slideshow One', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 3, 14),
(16, 'Page', '2013-09-08 07:32:08', '2013-09-08 07:35:28', 'exhibitions', 'Exhibitions', NULL, '<table border="0"><tbody><tr><td>Between Tides 2007 - 2013</td>\n<td>Annual one day community art even, Westmere, Auckland</td>\n</tr><tr><td>Third Course - Art on Plate April 2012</td>\n<td>Northart Community Arts centre Northcote Auckland</td>\n</tr><tr><td>Affairs of the Heart March 2010</td>\n<td>blurb</td>\n</tr><tr><td>North Art Members Exhibition April 2007-09</td>\n<td>blurb</td>\n</tr><tr><td>Blue Askew October 2009</td>\n<td>blurb</td>\n</tr><tr><td>Not Just Wallpaper October 2008</td>\n<td>blurb</td>\n</tr><tr><td>Strictly Bayview October 2008</td>\n<td>blurb</td>\n</tr><tr><td>Parallel Parade May 2008</td>\n<td>blurb</td>\n</tr><tr><td>Painted Plates April 2007</td>\n<td>blurb</td>\n</tr><tr><td>100x100x100 200x200x200 2003-04</td>\n<td>blurb</td>\n</tr><tr><td>Living is Easy... August 2002</td>\n<td>blurb</td>\n</tr></tbody></table>', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', 4, 0),
(17, 'GalleryPage', '2013-09-08 08:16:03', '2013-09-08 08:17:25', 'slideshow-two', 'Slideshow Two', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 5, 14);

-- --------------------------------------------------------

--
-- Table structure for table `SiteTree_EditorGroups`
--

CREATE TABLE `SiteTree_EditorGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SiteTree_ImageTracking`
--

CREATE TABLE `SiteTree_ImageTracking` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `FileID` int(11) NOT NULL DEFAULT '0',
  `FieldName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `FileID` (`FileID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SiteTree_LinkTracking`
--

CREATE TABLE `SiteTree_LinkTracking` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `ChildID` int(11) NOT NULL DEFAULT '0',
  `FieldName` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `ChildID` (`ChildID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SiteTree_Live`
--

CREATE TABLE `SiteTree_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('Page','ErrorPage','AboutPage','GalleryHolder','GalleryPage','ContactPage','HomePage','SiteTree','RedirectorPage','VirtualPage') DEFAULT 'Page',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `URLSegment` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `MenuTitle` varchar(100) DEFAULT NULL,
  `Content` mediumtext,
  `MetaTitle` varchar(255) DEFAULT NULL,
  `MetaDescription` mediumtext,
  `MetaKeywords` varchar(1024) DEFAULT NULL,
  `ExtraMeta` mediumtext,
  `ShowInMenus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ReportClass` varchar(50) DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `Version` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `URLSegment` (`URLSegment`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=23 ;

--
-- Dumping data for table `SiteTree_Live`
--

INSERT INTO `SiteTree_Live` (`ID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaTitle`, `MetaDescription`, `MetaKeywords`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `Version`, `ParentID`) VALUES
(1, 'HomePage', '2013-09-08 01:02:17', '2013-09-09 09:36:11', 'home', 'Home', NULL, '<h4>Studio:</h4>\n<p>149 Queent St<br>Northcote Point<br>Auckland 6027<br>New Zealand</p>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 19, 0),
(2, 'AboutPage', '2013-09-08 01:02:17', '2013-09-08 07:04:05', 'about', 'About', NULL, '<p><em><span class="itl">"I''m always searching for the ''a-ha'' moment when the balance is struck between the formal components of the object and the desired charisma and impact"</span></em></p>\n<p>Based in Auckland, New Zealand, light-designer and object-maker Phillippa Kenny has been creating light works for the past 9 years</p>\n<p>Phillippa''s lights are suited to commercial as well as domestic spaces, providing the local interest of an object d''art while also adding atmosphere through colour and tone. In designing her current serires of lights, Phillippa continues to gravitate toward the simple beauty of line &amp; pattern while incorporating more recent explorations into the interplay of texture and colour.</p>\n<p>Several of Phillippa''s artworks are included in the Wallace Arts Trust Collection, and she has exhibited in a number of Auckland galleries and outdoor site-specific exhibitions.</p>\n<table style="width: 400px; margin: 25px 0px 10px;" border="0"><tbody><tr><td>\n<h4>Awards:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>First place - Untitled 2001</span></td>\n<td><span>Depot Art Award, Depot Art &amp; Music Space, Devonport, Auckland</span></td>\n</tr></tbody></table><table style="width: 400px;" border="0"><tbody><tr><td>\n<h4>Academic Qualifications:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>Bachelor of Visual Arts 2001</span></td>\n<td><span>Auckland University of Technology</span></td>\n</tr></tbody></table>', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 15, 0),
(4, 'ErrorPage', '2013-09-08 01:02:17', '2013-09-08 01:03:15', 'page-not-found', 'Page not found', NULL, '<p>Sorry, it seems you were trying to access a page that doesn''t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>', NULL, NULL, NULL, NULL, 0, 0, 7, 0, 0, NULL, 'Inherit', 'Inherit', 1, 0),
(5, 'ErrorPage', '2013-09-08 01:02:18', '2013-09-08 01:02:18', 'server-error', 'Server error', NULL, '<p>Sorry, there was a problem with handling your request.</p>', NULL, NULL, NULL, NULL, 0, 0, 8, 0, 0, NULL, 'Inherit', 'Inherit', 1, 0),
(11, 'GalleryHolder', '2013-09-08 04:51:32', '2013-09-08 06:01:24', 'gallery', 'Gallery', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 5, 0, 0, NULL, 'Inherit', 'Inherit', 4, 0),
(12, 'GalleryPage', '2013-09-08 04:52:41', '2013-09-08 05:03:07', 'image-one', 'Image One', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 6, 11),
(13, 'GalleryPage', '2013-09-08 05:09:58', '2013-09-08 05:10:18', 'image-two', 'Image Two', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 3, 11),
(14, 'GalleryHolder', '2013-09-08 06:22:22', '2013-09-08 06:26:27', 'slideshow', 'Slideshow', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 5, 0),
(15, 'GalleryPage', '2013-09-08 06:22:45', '2013-09-08 06:23:48', 'slideshow-one', 'Slideshow One', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 3, 14),
(16, 'Page', '2013-09-08 07:32:08', '2013-09-08 07:35:28', 'exhibitions', 'Exhibitions', NULL, '<table border="0"><tbody><tr><td>Between Tides 2007 - 2013</td>\n<td>Annual one day community art even, Westmere, Auckland</td>\n</tr><tr><td>Third Course - Art on Plate April 2012</td>\n<td>Northart Community Arts centre Northcote Auckland</td>\n</tr><tr><td>Affairs of the Heart March 2010</td>\n<td>blurb</td>\n</tr><tr><td>North Art Members Exhibition April 2007-09</td>\n<td>blurb</td>\n</tr><tr><td>Blue Askew October 2009</td>\n<td>blurb</td>\n</tr><tr><td>Not Just Wallpaper October 2008</td>\n<td>blurb</td>\n</tr><tr><td>Strictly Bayview October 2008</td>\n<td>blurb</td>\n</tr><tr><td>Parallel Parade May 2008</td>\n<td>blurb</td>\n</tr><tr><td>Painted Plates April 2007</td>\n<td>blurb</td>\n</tr><tr><td>100x100x100 200x200x200 2003-04</td>\n<td>blurb</td>\n</tr><tr><td>Living is Easy... August 2002</td>\n<td>blurb</td>\n</tr></tbody></table>', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', 4, 0),
(17, 'GalleryPage', '2013-09-08 08:16:03', '2013-09-08 08:17:25', 'slideshow-two', 'Slideshow Two', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 5, 14);

-- --------------------------------------------------------

--
-- Table structure for table `SiteTree_versions`
--

CREATE TABLE `SiteTree_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `WasPublished` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `AuthorID` int(11) NOT NULL DEFAULT '0',
  `PublisherID` int(11) NOT NULL DEFAULT '0',
  `ClassName` enum('Page','ErrorPage','AboutPage','GalleryHolder','GalleryPage','ContactPage','HomePage','SiteTree','RedirectorPage','VirtualPage') DEFAULT 'Page',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `URLSegment` varchar(255) DEFAULT NULL,
  `Title` varchar(255) DEFAULT NULL,
  `MenuTitle` varchar(100) DEFAULT NULL,
  `Content` mediumtext,
  `MetaTitle` varchar(255) DEFAULT NULL,
  `MetaDescription` mediumtext,
  `MetaKeywords` varchar(1024) DEFAULT NULL,
  `ExtraMeta` mediumtext,
  `ShowInMenus` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ShowInSearch` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Sort` int(11) NOT NULL DEFAULT '0',
  `HasBrokenFile` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HasBrokenLink` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `ReportClass` varchar(50) DEFAULT NULL,
  `CanViewType` enum('Anyone','LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `CanEditType` enum('LoggedInUsers','OnlyTheseUsers','Inherit') DEFAULT 'Inherit',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `AuthorID` (`AuthorID`),
  KEY `PublisherID` (`PublisherID`),
  KEY `ParentID` (`ParentID`),
  KEY `URLSegment` (`URLSegment`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=106 ;

--
-- Dumping data for table `SiteTree_versions`
--

INSERT INTO `SiteTree_versions` (`ID`, `RecordID`, `Version`, `WasPublished`, `AuthorID`, `PublisherID`, `ClassName`, `Created`, `LastEdited`, `URLSegment`, `Title`, `MenuTitle`, `Content`, `MetaTitle`, `MetaDescription`, `MetaKeywords`, `ExtraMeta`, `ShowInMenus`, `ShowInSearch`, `Sort`, `HasBrokenFile`, `HasBrokenLink`, `ReportClass`, `CanViewType`, `CanEditType`, `ParentID`) VALUES
(1, 1, 1, 1, 0, 0, 'Page', '2013-09-08 01:02:17', '2013-09-08 01:02:17', 'home', 'Home', NULL, '<p>Welcome to SilverStripe! This is the default homepage. You can edit this page by opening <a href="admin/">the CMS</a>. You can now access the <a href="http://doc.silverstripe.org">developer documentation</a>, or begin <a href="http://doc.silverstripe.org/doku.php?id=tutorials">the tutorials.</a></p>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(2, 2, 1, 1, 0, 0, 'Page', '2013-09-08 01:02:17', '2013-09-08 01:02:17', 'about-us', 'About Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(3, 3, 1, 1, 0, 0, 'Page', '2013-09-08 01:02:17', '2013-09-08 01:02:17', 'contact-us', 'Contact Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(4, 4, 1, 1, 0, 0, 'ErrorPage', '2013-09-08 01:02:17', '2013-09-08 01:02:17', 'page-not-found', 'Page not found', NULL, '<p>Sorry, it seems you were trying to access a page that doesn''t exist.</p><p>Please check the spelling of the URL you were trying to access and try again.</p>', NULL, NULL, NULL, NULL, 0, 0, 4, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(5, 5, 1, 1, 0, 0, 'ErrorPage', '2013-09-08 01:02:18', '2013-09-08 01:02:18', 'server-error', 'Server error', NULL, '<p>Sorry, there was a problem with handling your request.</p>', NULL, NULL, NULL, NULL, 0, 0, 5, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(6, 1, 2, 1, 1, 1, 'Page', '2013-09-08 01:02:17', '2013-09-08 01:23:07', 'home', 'Home', NULL, '<p>Welcome to PK Light</p>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(7, 2, 2, 1, 1, 1, 'Page', '2013-09-08 01:02:17', '2013-09-08 01:56:38', 'about', 'About', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(8, 2, 3, 1, 1, 1, 'AboutPage', '2013-09-08 01:02:17', '2013-09-08 01:56:50', 'about', 'About', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(9, 2, 4, 0, 1, 0, 'AboutPage', '2013-09-08 01:02:17', '2013-09-08 01:57:37', 'about', 'About', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(10, 2, 5, 1, 1, 1, 'AboutPage', '2013-09-08 01:02:17', '2013-09-08 02:08:18', 'about', 'About', NULL, '<p><em><span class="itl">"I''m always searching for the ''a-ha'' moment when the balance is struck between the formal components of the object and the desired charisma and impact"</span></em></p>\n<p>Based in Auckland, New Zealand, light-designer and object-maker Phillippa Kenny has been creating light works for the past 9 years</p>\n<p>Phillippa''s lights are suited to commercial as well as domestic spaces, providing the local interest of an object d''art while also adding atmosphere through colour and tone. In designing her current serires of lights, Phillippa continues to gravitate toward the simple beauty of line &amp; pattern while incorporating more recent explorations into the interplay of texture and colour.</p>\n<p>Several of Phillippa''s artworks are included in the Wallace Arts Trust Collection, and she has exhibited in a number of Auckland galleries and outdoor site-specific exhibitions.</p>\n<table style="width: 400px; margin: 25px 0px 0px;" border="0"><tbody><tr><td>\n<h4>Awards:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>First place - Untitled 2001</span></td>\n<td><span>Depot Art Award, Depot Art &amp; Music Space, Devonport, Auckland</span></td>\n</tr></tbody></table><table style="width: 400px;" border="0"><tbody><tr><td>\n<h4>Academic Qualifications:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>Bachelor of Visual Arts 2001</span></td>\n<td><span>Auckland University of Technology</span></td>\n</tr></tbody></table>', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(11, 6, 1, 0, 1, 0, 'Page', '2013-09-08 02:19:30', '2013-09-08 02:19:30', 'new-page', 'New Page', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(12, 6, 2, 1, 1, 1, 'Page', '2013-09-08 02:19:30', '2013-09-08 02:19:46', 'sections', 'Sections', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(13, 6, 3, 0, 1, 0, 'Page', '2013-09-08 02:19:30', '2013-09-08 02:19:51', 'sections', 'Sections', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(14, 1, 3, 0, 1, 0, 'Page', '2013-09-08 01:02:17', '2013-09-08 02:19:54', 'home', 'Home', NULL, '<p>Welcome to PK Light</p>', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 6),
(15, 1, 4, 0, 1, 0, 'Page', '2013-09-08 01:02:17', '2013-09-08 02:19:54', 'home', 'Home', NULL, '<p>Welcome to PK Light</p>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 6),
(16, 2, 6, 0, 1, 0, 'AboutPage', '2013-09-08 01:02:17', '2013-09-08 02:19:56', 'about', 'About', NULL, '<p><em><span class="itl">"I''m always searching for the ''a-ha'' moment when the balance is struck between the formal components of the object and the desired charisma and impact"</span></em></p>\n<p>Based in Auckland, New Zealand, light-designer and object-maker Phillippa Kenny has been creating light works for the past 9 years</p>\n<p>Phillippa''s lights are suited to commercial as well as domestic spaces, providing the local interest of an object d''art while also adding atmosphere through colour and tone. In designing her current serires of lights, Phillippa continues to gravitate toward the simple beauty of line &amp; pattern while incorporating more recent explorations into the interplay of texture and colour.</p>\n<p>Several of Phillippa''s artworks are included in the Wallace Arts Trust Collection, and she has exhibited in a number of Auckland galleries and outdoor site-specific exhibitions.</p>\n<table style="width: 400px; margin: 25px 0px 0px;" border="0"><tbody><tr><td>\n<h4>Awards:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>First place - Untitled 2001</span></td>\n<td><span>Depot Art Award, Depot Art &amp; Music Space, Devonport, Auckland</span></td>\n</tr></tbody></table><table style="width: 400px;" border="0"><tbody><tr><td>\n<h4>Academic Qualifications:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>Bachelor of Visual Arts 2001</span></td>\n<td><span>Auckland University of Technology</span></td>\n</tr></tbody></table>', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 6),
(17, 2, 7, 0, 1, 0, 'AboutPage', '2013-09-08 01:02:17', '2013-09-08 02:19:56', 'about', 'About', NULL, '<p><em><span class="itl">"I''m always searching for the ''a-ha'' moment when the balance is struck between the formal components of the object and the desired charisma and impact"</span></em></p>\n<p>Based in Auckland, New Zealand, light-designer and object-maker Phillippa Kenny has been creating light works for the past 9 years</p>\n<p>Phillippa''s lights are suited to commercial as well as domestic spaces, providing the local interest of an object d''art while also adding atmosphere through colour and tone. In designing her current serires of lights, Phillippa continues to gravitate toward the simple beauty of line &amp; pattern while incorporating more recent explorations into the interplay of texture and colour.</p>\n<p>Several of Phillippa''s artworks are included in the Wallace Arts Trust Collection, and she has exhibited in a number of Auckland galleries and outdoor site-specific exhibitions.</p>\n<table style="width: 400px; margin: 25px 0px 0px;" border="0"><tbody><tr><td>\n<h4>Awards:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>First place - Untitled 2001</span></td>\n<td><span>Depot Art Award, Depot Art &amp; Music Space, Devonport, Auckland</span></td>\n</tr></tbody></table><table style="width: 400px;" border="0"><tbody><tr><td>\n<h4>Academic Qualifications:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>Bachelor of Visual Arts 2001</span></td>\n<td><span>Auckland University of Technology</span></td>\n</tr></tbody></table>', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 6),
(18, 3, 2, 0, 1, 0, 'Page', '2013-09-08 01:02:17', '2013-09-08 02:19:58', 'contact-us', 'Contact Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(19, 3, 3, 0, 1, 0, 'Page', '2013-09-08 01:02:17', '2013-09-08 02:20:00', 'contact-us', 'Contact Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 6),
(20, 3, 4, 0, 1, 0, 'Page', '2013-09-08 01:02:17', '2013-09-08 02:20:00', 'contact-us', 'Contact Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 6),
(21, 2, 8, 0, 1, 0, 'AboutPage', '2013-09-08 01:02:17', '2013-09-08 02:20:09', 'about', 'About', NULL, '<p><em><span class="itl">"I''m always searching for the ''a-ha'' moment when the balance is struck between the formal components of the object and the desired charisma and impact"</span></em></p>\n<p>Based in Auckland, New Zealand, light-designer and object-maker Phillippa Kenny has been creating light works for the past 9 years</p>\n<p>Phillippa''s lights are suited to commercial as well as domestic spaces, providing the local interest of an object d''art while also adding atmosphere through colour and tone. In designing her current serires of lights, Phillippa continues to gravitate toward the simple beauty of line &amp; pattern while incorporating more recent explorations into the interplay of texture and colour.</p>\n<p>Several of Phillippa''s artworks are included in the Wallace Arts Trust Collection, and she has exhibited in a number of Auckland galleries and outdoor site-specific exhibitions.</p>\n<table style="width: 400px; margin: 25px 0px 0px;" border="0"><tbody><tr><td>\n<h4>Awards:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>First place - Untitled 2001</span></td>\n<td><span>Depot Art Award, Depot Art &amp; Music Space, Devonport, Auckland</span></td>\n</tr></tbody></table><table style="width: 400px;" border="0"><tbody><tr><td>\n<h4>Academic Qualifications:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>Bachelor of Visual Arts 2001</span></td>\n<td><span>Auckland University of Technology</span></td>\n</tr></tbody></table>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 6),
(22, 2, 9, 0, 1, 0, 'AboutPage', '2013-09-08 01:02:17', '2013-09-08 02:20:11', 'about', 'About', NULL, '<p><em><span class="itl">"I''m always searching for the ''a-ha'' moment when the balance is struck between the formal components of the object and the desired charisma and impact"</span></em></p>\n<p>Based in Auckland, New Zealand, light-designer and object-maker Phillippa Kenny has been creating light works for the past 9 years</p>\n<p>Phillippa''s lights are suited to commercial as well as domestic spaces, providing the local interest of an object d''art while also adding atmosphere through colour and tone. In designing her current serires of lights, Phillippa continues to gravitate toward the simple beauty of line &amp; pattern while incorporating more recent explorations into the interplay of texture and colour.</p>\n<p>Several of Phillippa''s artworks are included in the Wallace Arts Trust Collection, and she has exhibited in a number of Auckland galleries and outdoor site-specific exhibitions.</p>\n<table style="width: 400px; margin: 25px 0px 0px;" border="0"><tbody><tr><td>\n<h4>Awards:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>First place - Untitled 2001</span></td>\n<td><span>Depot Art Award, Depot Art &amp; Music Space, Devonport, Auckland</span></td>\n</tr></tbody></table><table style="width: 400px;" border="0"><tbody><tr><td>\n<h4>Academic Qualifications:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>Bachelor of Visual Arts 2001</span></td>\n<td><span>Auckland University of Technology</span></td>\n</tr></tbody></table>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 1),
(23, 2, 10, 0, 1, 0, 'AboutPage', '2013-09-08 01:02:17', '2013-09-08 02:20:16', 'about', 'About', NULL, '<p><em><span class="itl">"I''m always searching for the ''a-ha'' moment when the balance is struck between the formal components of the object and the desired charisma and impact"</span></em></p>\n<p>Based in Auckland, New Zealand, light-designer and object-maker Phillippa Kenny has been creating light works for the past 9 years</p>\n<p>Phillippa''s lights are suited to commercial as well as domestic spaces, providing the local interest of an object d''art while also adding atmosphere through colour and tone. In designing her current serires of lights, Phillippa continues to gravitate toward the simple beauty of line &amp; pattern while incorporating more recent explorations into the interplay of texture and colour.</p>\n<p>Several of Phillippa''s artworks are included in the Wallace Arts Trust Collection, and she has exhibited in a number of Auckland galleries and outdoor site-specific exhibitions.</p>\n<table style="width: 400px; margin: 25px 0px 0px;" border="0"><tbody><tr><td>\n<h4>Awards:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>First place - Untitled 2001</span></td>\n<td><span>Depot Art Award, Depot Art &amp; Music Space, Devonport, Auckland</span></td>\n</tr></tbody></table><table style="width: 400px;" border="0"><tbody><tr><td>\n<h4>Academic Qualifications:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>Bachelor of Visual Arts 2001</span></td>\n<td><span>Auckland University of Technology</span></td>\n</tr></tbody></table>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 6),
(24, 2, 11, 0, 1, 0, 'AboutPage', '2013-09-08 01:02:17', '2013-09-08 02:20:16', 'about', 'About', NULL, '<p><em><span class="itl">"I''m always searching for the ''a-ha'' moment when the balance is struck between the formal components of the object and the desired charisma and impact"</span></em></p>\n<p>Based in Auckland, New Zealand, light-designer and object-maker Phillippa Kenny has been creating light works for the past 9 years</p>\n<p>Phillippa''s lights are suited to commercial as well as domestic spaces, providing the local interest of an object d''art while also adding atmosphere through colour and tone. In designing her current serires of lights, Phillippa continues to gravitate toward the simple beauty of line &amp; pattern while incorporating more recent explorations into the interplay of texture and colour.</p>\n<p>Several of Phillippa''s artworks are included in the Wallace Arts Trust Collection, and she has exhibited in a number of Auckland galleries and outdoor site-specific exhibitions.</p>\n<table style="width: 400px; margin: 25px 0px 0px;" border="0"><tbody><tr><td>\n<h4>Awards:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>First place - Untitled 2001</span></td>\n<td><span>Depot Art Award, Depot Art &amp; Music Space, Devonport, Auckland</span></td>\n</tr></tbody></table><table style="width: 400px;" border="0"><tbody><tr><td>\n<h4>Academic Qualifications:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>Bachelor of Visual Arts 2001</span></td>\n<td><span>Auckland University of Technology</span></td>\n</tr></tbody></table>', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 6),
(25, 1, 5, 0, 1, 0, 'Page', '2013-09-08 01:02:17', '2013-09-08 02:28:00', 'home', 'Home', NULL, '<p>Welcome to PK Light</p>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(26, 2, 12, 1, 1, 1, 'AboutPage', '2013-09-08 01:02:17', '2013-09-08 02:28:02', 'about', 'About', NULL, '<p><em><span class="itl">"I''m always searching for the ''a-ha'' moment when the balance is struck between the formal components of the object and the desired charisma and impact"</span></em></p>\n<p>Based in Auckland, New Zealand, light-designer and object-maker Phillippa Kenny has been creating light works for the past 9 years</p>\n<p>Phillippa''s lights are suited to commercial as well as domestic spaces, providing the local interest of an object d''art while also adding atmosphere through colour and tone. In designing her current serires of lights, Phillippa continues to gravitate toward the simple beauty of line &amp; pattern while incorporating more recent explorations into the interplay of texture and colour.</p>\n<p>Several of Phillippa''s artworks are included in the Wallace Arts Trust Collection, and she has exhibited in a number of Auckland galleries and outdoor site-specific exhibitions.</p>\n<table style="width: 400px; margin: 25px 0px 0px;" border="0"><tbody><tr><td>\n<h4>Awards:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>First place - Untitled 2001</span></td>\n<td><span>Depot Art Award, Depot Art &amp; Music Space, Devonport, Auckland</span></td>\n</tr></tbody></table><table style="width: 400px;" border="0"><tbody><tr><td>\n<h4>Academic Qualifications:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>Bachelor of Visual Arts 2001</span></td>\n<td><span>Auckland University of Technology</span></td>\n</tr></tbody></table>', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(27, 3, 5, 1, 1, 1, 'Page', '2013-09-08 01:02:17', '2013-09-08 02:28:03', 'contact-us', 'Contact Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(28, 1, 6, 1, 1, 1, 'Page', '2013-09-08 01:02:17', '2013-09-08 02:28:20', 'home', 'Home', NULL, '<p>Welcome to PK Light</p>', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(29, 2, 13, 0, 1, 0, 'AboutPage', '2013-09-08 01:02:17', '2013-09-08 03:14:08', 'about', 'About', NULL, '<p><em><span class="itl">"I''m always searching for the ''a-ha'' moment when the balance is struck between the formal components of the object and the desired charisma and impact"</span></em></p>\n<p>Based in Auckland, New Zealand, light-designer and object-maker Phillippa Kenny has been creating light works for the past 9 years</p>\n<p>Phillippa''s lights are suited to commercial as well as domestic spaces, providing the local interest of an object d''art while also adding atmosphere through colour and tone. In designing her current serires of lights, Phillippa continues to gravitate toward the simple beauty of line &amp; pattern while incorporating more recent explorations into the interplay of texture and colour.</p>\n<p>Several of Phillippa''s artworks are included in the Wallace Arts Trust Collection, and she has exhibited in a number of Auckland galleries and outdoor site-specific exhibitions.</p>\n<table style="width: 400px; margin: 25px 0px 0px;" border="0"><tbody><tr><td>\n<h4>Awards:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>First place - Untitled 2001</span></td>\n<td><span>Depot Art Award, Depot Art &amp; Music Space, Devonport, Auckland</span></td>\n</tr></tbody></table><table style="width: 400px;" border="0"><tbody><tr><td>\n<h4>Academic Qualifications:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>Bachelor of Visual Arts 2001</span></td>\n<td><span>Auckland University of Technology</span></td>\n</tr></tbody></table>', NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(30, 7, 1, 0, 1, 0, '', '2013-09-08 04:11:36', '2013-09-08 04:11:36', 'new-gallery-page', 'New Gallery Page', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(31, 8, 1, 0, 1, 0, '', '2013-09-08 04:17:05', '2013-09-08 04:17:05', 'new-gallery-page-2', 'New Gallery Page', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(32, 9, 1, 0, 1, 0, 'Page', '2013-09-08 04:18:46', '2013-09-08 04:18:46', 'new-page', 'New Page', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(33, 9, 2, 1, 1, 1, 'Page', '2013-09-08 04:18:46', '2013-09-08 04:18:54', 'gallery', 'Gallery', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(34, 9, 3, 0, 1, 0, 'Page', '2013-09-08 04:18:46', '2013-09-08 04:19:01', 'gallery', 'Gallery', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(35, 9, 4, 1, 1, 1, '', '2013-09-08 04:18:46', '2013-09-08 04:43:10', 'gallery', 'Gallery', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(36, 9, 5, 1, 1, 1, '', '2013-09-08 04:18:46', '2013-09-08 04:44:04', 'gallery', 'Gallery', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(37, 10, 1, 0, 1, 0, 'Page', '2013-09-08 04:45:18', '2013-09-08 04:45:18', 'new-page', 'New Page', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(38, 10, 2, 1, 1, 1, 'Page', '2013-09-08 04:45:18', '2013-09-08 04:45:23', 'gallery', 'Gallery', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(39, 10, 3, 0, 1, 0, 'Page', '2013-09-08 04:45:18', '2013-09-08 04:45:30', 'gallery', 'Gallery', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(40, 10, 4, 1, 1, 1, 'GalleryHolder', '2013-09-08 04:45:18', '2013-09-08 04:50:29', 'gallery', 'Gallery', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(41, 11, 1, 0, 1, 0, 'GalleryHolder', '2013-09-08 04:51:32', '2013-09-08 04:51:32', 'new-gallery-holder', 'New Gallery Holder', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(42, 11, 2, 1, 1, 1, 'GalleryHolder', '2013-09-08 04:51:32', '2013-09-08 04:51:37', 'new-gallery-holder', 'Gallery', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 7, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(43, 11, 3, 1, 1, 1, 'GalleryHolder', '2013-09-08 04:51:32', '2013-09-08 04:51:46', 'new-gallery-holder', 'Gallery', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(44, 12, 1, 0, 1, 0, 'GalleryPage', '2013-09-08 04:52:41', '2013-09-08 04:52:41', 'new-gallery-page', 'New Gallery Page', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 11),
(45, 12, 2, 0, 1, 0, 'GalleryPage', '2013-09-08 04:52:41', '2013-09-08 04:54:39', 'new-gallery-page', 'New Gallery Page', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 11),
(46, 12, 3, 1, 1, 1, 'GalleryPage', '2013-09-08 04:52:41', '2013-09-08 04:54:41', 'image-one', 'Image One', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 11),
(47, 12, 4, 1, 1, 1, 'GalleryPage', '2013-09-08 04:52:41', '2013-09-08 05:00:50', 'image-one', 'Image One', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 11),
(48, 12, 5, 0, 1, 0, 'GalleryPage', '2013-09-08 04:52:41', '2013-09-08 05:03:00', 'image-one', 'Image One', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 11),
(49, 12, 6, 1, 1, 1, 'GalleryPage', '2013-09-08 04:52:41', '2013-09-08 05:03:07', 'image-one', 'Image One', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 11),
(50, 13, 1, 0, 1, 0, 'GalleryPage', '2013-09-08 05:09:58', '2013-09-08 05:09:58', 'new-gallery-page', 'New Gallery Page', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 11),
(51, 13, 2, 0, 1, 0, 'GalleryPage', '2013-09-08 05:09:58', '2013-09-08 05:10:15', 'new-gallery-page', 'New Gallery Page', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 11),
(52, 13, 3, 1, 1, 1, 'GalleryPage', '2013-09-08 05:09:58', '2013-09-08 05:10:18', 'image-two', 'Image Two', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 11),
(53, 11, 4, 1, 1, 1, 'GalleryHolder', '2013-09-08 04:51:32', '2013-09-08 06:01:23', 'gallery', 'Gallery', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(54, 14, 1, 0, 1, 0, 'GalleryHolder', '2013-09-08 06:22:22', '2013-09-08 06:22:22', 'new-gallery-holder', 'New Gallery Holder', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(55, 14, 2, 1, 1, 1, 'GalleryHolder', '2013-09-08 06:22:22', '2013-09-08 06:22:27', 'new-gallery-holder', 'Slideshow', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(56, 14, 3, 0, 1, 0, 'GalleryHolder', '2013-09-08 06:22:22', '2013-09-08 06:22:31', 'new-gallery-holder', 'Slideshow', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(57, 14, 4, 1, 1, 1, 'GalleryHolder', '2013-09-08 06:22:22', '2013-09-08 06:22:38', 'new-gallery-holder', 'Slideshow', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(58, 15, 1, 0, 1, 0, 'GalleryPage', '2013-09-08 06:22:45', '2013-09-08 06:22:45', 'new-gallery-page', 'New Gallery Page', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 14),
(59, 15, 2, 0, 1, 0, 'GalleryPage', '2013-09-08 06:22:45', '2013-09-08 06:23:25', 'new-gallery-page', 'New Gallery Page', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 14),
(60, 15, 3, 1, 1, 1, 'GalleryPage', '2013-09-08 06:22:45', '2013-09-08 06:23:48', 'slideshow-one', 'Slideshow One', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 14),
(61, 14, 5, 1, 1, 1, 'GalleryHolder', '2013-09-08 06:22:22', '2013-09-08 06:26:27', 'slideshow', 'Slideshow', NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(62, 2, 14, 1, 1, 1, 'AboutPage', '2013-09-08 01:02:17', '2013-09-08 07:03:20', 'about', 'About', NULL, '<p><em><span class="itl">"I''m always searching for the ''a-ha'' moment when the balance is struck between the formal components of the object and the desired charisma and impact"</span></em></p>\n<p>Based in Auckland, New Zealand, light-designer and object-maker Phillippa Kenny has been creating light works for the past 9 years</p>\n<p>Phillippa''s lights are suited to commercial as well as domestic spaces, providing the local interest of an object d''art while also adding atmosphere through colour and tone. In designing her current serires of lights, Phillippa continues to gravitate toward the simple beauty of line &amp; pattern while incorporating more recent explorations into the interplay of texture and colour.</p>\n<p>Several of Phillippa''s artworks are included in the Wallace Arts Trust Collection, and she has exhibited in a number of Auckland galleries and outdoor site-specific exhibitions.</p>\n<table style="width: 400px; margin: 25px 0px;" border="0"><tbody><tr><td>\n<h4>Awards:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>First place - Untitled 2001</span></td>\n<td><span>Depot Art Award, Depot Art &amp; Music Space, Devonport, Auckland</span></td>\n</tr></tbody></table><table style="width: 400px;" border="0"><tbody><tr><td>\n<h4>Academic Qualifications:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>Bachelor of Visual Arts 2001</span></td>\n<td><span>Auckland University of Technology</span></td>\n</tr></tbody></table>', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(63, 2, 15, 1, 1, 1, 'AboutPage', '2013-09-08 01:02:17', '2013-09-08 07:04:05', 'about', 'About', NULL, '<p><em><span class="itl">"I''m always searching for the ''a-ha'' moment when the balance is struck between the formal components of the object and the desired charisma and impact"</span></em></p>\n<p>Based in Auckland, New Zealand, light-designer and object-maker Phillippa Kenny has been creating light works for the past 9 years</p>\n<p>Phillippa''s lights are suited to commercial as well as domestic spaces, providing the local interest of an object d''art while also adding atmosphere through colour and tone. In designing her current serires of lights, Phillippa continues to gravitate toward the simple beauty of line &amp; pattern while incorporating more recent explorations into the interplay of texture and colour.</p>\n<p>Several of Phillippa''s artworks are included in the Wallace Arts Trust Collection, and she has exhibited in a number of Auckland galleries and outdoor site-specific exhibitions.</p>\n<table style="width: 400px; margin: 25px 0px 10px;" border="0"><tbody><tr><td>\n<h4>Awards:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>First place - Untitled 2001</span></td>\n<td><span>Depot Art Award, Depot Art &amp; Music Space, Devonport, Auckland</span></td>\n</tr></tbody></table><table style="width: 400px;" border="0"><tbody><tr><td>\n<h4>Academic Qualifications:</h4>\n</td>\n<td> </td>\n</tr><tr><td><span>Bachelor of Visual Arts 2001</span></td>\n<td><span>Auckland University of Technology</span></td>\n</tr></tbody></table>', NULL, NULL, NULL, NULL, 1, 1, 3, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(64, 16, 1, 0, 1, 0, 'Page', '2013-09-08 07:32:08', '2013-09-08 07:32:08', 'new-page', 'New Page', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 8, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(65, 16, 2, 0, 1, 0, 'Page', '2013-09-08 07:32:08', '2013-09-08 07:32:14', 'new-page', 'New Page', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(66, 16, 3, 1, 1, 1, 'Page', '2013-09-08 07:32:08', '2013-09-08 07:33:11', 'exhibitions', 'Exhibitions', NULL, '<table border="0"><thead></thead><tbody><tr><td>Between Tides 2007 - 2013</td>\n<td>Annual one day community art even, Westmere, Auckland</td>\n</tr><tr><td>Third Course - Art on Plate April 2012</td>\n<td>Northart Community Arts centre Northcote Auckland</td>\n</tr><tr><td>Affairs of the Heart March 2010</td>\n<td>blurb</td>\n</tr><tr><td>North Art Members Exhibition April 2007-09</td>\n<td>blurb</td>\n</tr><tr><td>Blue Askew October 2009</td>\n<td>blurb</td>\n</tr><tr><td>Not Just Wallpaper October 2008</td>\n<td>blurb</td>\n</tr><tr><td>Strictly Bayview October 2008</td>\n<td>blurb</td>\n</tr><tr><td>Parallel Parade May 2008</td>\n<td>blurb</td>\n</tr><tr><td>Painted Plates April 2007</td>\n<td>blurb</td>\n</tr><tr><td>100x100x100 200x200x200 2003-04</td>\n<td>blurb</td>\n</tr><tr><td>Living is Easy... August 2002</td>\n<td>blurb</td>\n</tr></tbody></table>', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(67, 16, 4, 1, 1, 1, 'Page', '2013-09-08 07:32:08', '2013-09-08 07:35:28', 'exhibitions', 'Exhibitions', NULL, '<table border="0"><tbody><tr><td>Between Tides 2007 - 2013</td>\n<td>Annual one day community art even, Westmere, Auckland</td>\n</tr><tr><td>Third Course - Art on Plate April 2012</td>\n<td>Northart Community Arts centre Northcote Auckland</td>\n</tr><tr><td>Affairs of the Heart March 2010</td>\n<td>blurb</td>\n</tr><tr><td>North Art Members Exhibition April 2007-09</td>\n<td>blurb</td>\n</tr><tr><td>Blue Askew October 2009</td>\n<td>blurb</td>\n</tr><tr><td>Not Just Wallpaper October 2008</td>\n<td>blurb</td>\n</tr><tr><td>Strictly Bayview October 2008</td>\n<td>blurb</td>\n</tr><tr><td>Parallel Parade May 2008</td>\n<td>blurb</td>\n</tr><tr><td>Painted Plates April 2007</td>\n<td>blurb</td>\n</tr><tr><td>100x100x100 200x200x200 2003-04</td>\n<td>blurb</td>\n</tr><tr><td>Living is Easy... August 2002</td>\n<td>blurb</td>\n</tr></tbody></table>', NULL, NULL, NULL, NULL, 1, 1, 4, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(68, 17, 1, 0, 1, 0, 'GalleryPage', '2013-09-08 08:16:03', '2013-09-08 08:16:03', 'new-gallery-page', 'New Gallery Page', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 14),
(69, 17, 2, 0, 1, 0, 'GalleryPage', '2013-09-08 08:16:03', '2013-09-08 08:16:15', 'new-gallery-page', 'New Gallery Page', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 14),
(70, 17, 3, 1, 1, 1, 'GalleryPage', '2013-09-08 08:16:03', '2013-09-08 08:16:17', 'slideshow-two', 'Slideshow Two', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 2, 0, 0, NULL, 'Inherit', 'Inherit', 14),
(71, 17, 4, 0, 1, 0, 'GalleryPage', '2013-09-08 08:16:03', '2013-09-08 08:17:20', 'slideshow-two', 'Slideshow Two', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 14),
(72, 17, 5, 1, 1, 1, 'GalleryPage', '2013-09-08 08:16:03', '2013-09-08 08:17:25', 'slideshow-two', 'Slideshow Two', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 14),
(73, 3, 6, 1, 1, 1, 'ContactPage', '2013-09-08 01:02:17', '2013-09-08 09:43:18', 'contact-us', 'Contact Us', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(74, 3, 7, 1, 1, 1, 'ContactPage', '2013-09-08 01:02:17', '2013-09-08 09:45:51', 'contact', 'Contact', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(75, 3, 8, 1, 1, 1, 'Page', '2013-09-08 01:02:17', '2013-09-08 11:20:20', 'contact', 'Contact', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(76, 3, 9, 1, 1, 1, 'ContactPage', '2013-09-08 01:02:17', '2013-09-08 11:28:57', 'contact', 'Contact', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(77, 3, 10, 1, 1, 1, 'Page', '2013-09-08 01:02:17', '2013-09-08 11:32:28', 'contact', 'Contact', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(78, 3, 11, 1, 1, 1, 'ContactPage', '2013-09-08 01:02:17', '2013-09-08 11:33:20', 'contact', 'Contact', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(79, 18, 1, 0, 1, 0, 'Page', '2013-09-08 11:51:45', '2013-09-08 11:51:45', 'new-page', 'New Page', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 9, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(80, 19, 1, 0, 1, 0, 'Page', '2013-09-08 11:54:54', '2013-09-08 11:54:54', 'new-page', 'New Page', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 9, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(81, 1, 7, 1, 1, 1, 'ContactPage', '2013-09-08 01:02:17', '2013-09-08 11:55:21', 'home', 'Home', NULL, '<p>Welcome to PK Light</p>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(82, 1, 8, 1, 1, 1, '', '2013-09-08 01:02:17', '2013-09-08 11:57:01', 'home', 'Home', NULL, '<p>Welcome to PK Light</p>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(83, 3, 12, 1, 1, 1, '', '2013-09-08 01:02:17', '2013-09-08 11:57:23', 'contact', 'Contact', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(84, 1, 9, 1, 1, 1, 'Page', '2013-09-08 01:02:17', '2013-09-08 11:58:00', 'home', 'Home', NULL, '<p>Welcome to PK Light</p>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(85, 3, 13, 1, 1, 1, '', '2013-09-08 01:02:17', '2013-09-08 11:59:29', 'contact', 'Contact', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(86, 3, 14, 1, 1, 1, 'Page', '2013-09-08 01:02:17', '2013-09-08 12:00:45', 'contact', 'Contact', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(87, 3, 15, 1, 1, 1, '', '2013-09-08 01:02:17', '2013-09-08 12:02:17', 'contact', 'Contact', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(88, 3, 16, 1, 1, 1, 'ContactPage', '2013-09-08 01:02:17', '2013-09-08 12:03:31', 'contact', 'Contact', NULL, '<p>You can fill this page out with your own content, or delete it and create your own pages.<br></p>', NULL, NULL, NULL, NULL, 1, 1, 6, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(89, 1, 10, 1, 1, 1, 'ContactPage', '2013-09-08 01:02:17', '2013-09-08 12:05:13', 'home', 'Home', NULL, '<p>Welcome to PK Light</p>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(90, 1, 11, 1, 1, 1, 'Page', '2013-09-08 01:02:17', '2013-09-08 12:06:13', 'home', 'Home', NULL, '<p>Welcome to PK Light</p>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(91, 20, 1, 0, 1, 0, '', '2013-09-08 12:16:02', '2013-09-08 12:16:02', 'new-user-defined-form', 'New User Defined Form', NULL, '$UserDefinedForm', NULL, NULL, NULL, NULL, 1, 1, 9, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(92, 20, 2, 1, 1, 1, '', '2013-09-08 12:16:02', '2013-09-08 12:16:28', 'form-2', 'Custom form', NULL, '$UserDefinedForm', NULL, NULL, NULL, NULL, 1, 1, 9, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(93, 1, 12, 1, 1, 1, '', '2013-09-08 01:02:17', '2013-09-08 12:17:07', 'home', 'Home', NULL, '<p>Welcome to PK Light</p>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(94, 1, 13, 1, 1, 1, 'Page', '2013-09-08 01:02:17', '2013-09-08 12:17:17', 'home', 'Home', NULL, '<p>Welcome to PK Light</p>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(95, 21, 1, 1, 1, 1, '', '2013-09-08 12:17:29', '2013-09-08 12:17:29', 'new-user-defined-form', 'New User Defined Form', NULL, '$UserDefinedForm', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 1),
(96, 21, 2, 1, 1, 1, '', '2013-09-08 12:17:29', '2013-09-08 12:17:48', 'user-defined-form', 'User Defined Form', NULL, '$UserDefinedForm', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 1),
(97, 1, 14, 1, 1, 1, 'HomePage', '2013-09-08 01:02:17', '2013-09-08 12:28:04', 'home', 'Home', NULL, '<p>Welcome to PK Light</p>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(98, 1, 15, 1, 1, 1, 'Page', '2013-09-08 01:02:17', '2013-09-08 12:36:22', 'home', 'Home', NULL, '<p>Welcome to PK Light</p>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(99, 1, 16, 1, 1, 1, 'HomePage', '2013-09-08 01:02:17', '2013-09-08 12:42:07', 'home', 'Home', NULL, '<p>Welcome to PK Light</p>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(100, 22, 1, 0, 1, 0, 'Page', '2013-09-08 12:58:50', '2013-09-08 12:58:50', 'new-page', 'New Page', NULL, NULL, NULL, NULL, NULL, NULL, 1, 1, 9, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(101, 22, 2, 1, 1, 1, 'Page', '2013-09-08 12:58:50', '2013-09-08 12:59:03', 'contact-form', 'Contact Form', NULL, '<p>Done</p>', NULL, NULL, NULL, NULL, 1, 1, 9, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(102, 22, 3, 1, 1, 1, 'Page', '2013-09-08 12:58:50', '2013-09-08 12:59:22', 'contactform', 'Contactform', NULL, '<p>Done</p>', NULL, NULL, NULL, NULL, 1, 1, 9, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(103, 1, 17, 1, 1, 1, 'Page', '2013-09-08 01:02:17', '2013-09-09 08:26:16', 'home', 'Home', NULL, '<p>Welcome to PK Light</p>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(104, 1, 18, 1, 1, 1, 'HomePage', '2013-09-08 01:02:17', '2013-09-09 08:36:41', 'home', 'Home', NULL, '<p>Welcome to PK Light</p>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0),
(105, 1, 19, 1, 1, 1, 'HomePage', '2013-09-08 01:02:17', '2013-09-09 09:36:11', 'home', 'Home', NULL, '<h4>Studio:</h4>\n<p>149 Queent St<br>Northcote Point<br>Auckland 6027<br>New Zealand</p>', NULL, NULL, NULL, NULL, 1, 1, 1, 0, 0, NULL, 'Inherit', 'Inherit', 0);

-- --------------------------------------------------------

--
-- Table structure for table `SiteTree_ViewerGroups`
--

CREATE TABLE `SiteTree_ViewerGroups` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SiteTreeID` int(11) NOT NULL DEFAULT '0',
  `GroupID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SiteTreeID` (`SiteTreeID`),
  KEY `GroupID` (`GroupID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SubmittedFileField`
--

CREATE TABLE `SubmittedFileField` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `UploadedFileID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `UploadedFileID` (`UploadedFileID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SubmittedForm`
--

CREATE TABLE `SubmittedForm` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SubmittedForm') DEFAULT 'SubmittedForm',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `SubmittedByID` int(11) NOT NULL DEFAULT '0',
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `SubmittedByID` (`SubmittedByID`),
  KEY `ParentID` (`ParentID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `SubmittedFormField`
--

CREATE TABLE `SubmittedFormField` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('SubmittedFormField','SubmittedFileField') DEFAULT 'SubmittedFormField',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `Name` varchar(50) DEFAULT NULL,
  `Value` mediumtext,
  `Title` varchar(255) DEFAULT NULL,
  `ParentID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `ParentID` (`ParentID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `UserDefinedForm`
--

CREATE TABLE `UserDefinedForm` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SubmitButtonText` varchar(50) DEFAULT NULL,
  `OnCompleteMessage` mediumtext,
  `ShowClearButton` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DisableSaveSubmissions` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `EnableLiveValidation` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HideFieldLabels` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `UserDefinedForm`
--

INSERT INTO `UserDefinedForm` (`ID`, `SubmitButtonText`, `OnCompleteMessage`, `ShowClearButton`, `DisableSaveSubmissions`, `EnableLiveValidation`, `HideFieldLabels`, `Version`) VALUES
(1, NULL, NULL, 0, 0, 0, 0, 0),
(3, NULL, NULL, 0, 0, 1, 0, 0),
(20, NULL, '<p>Thanks, we''ve received your submission.</p>', 0, 0, 0, 0, 0),
(21, NULL, '<p>Thanks, we''ve received your submission.</p>', 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `UserDefinedForm_EmailRecipient`
--

CREATE TABLE `UserDefinedForm_EmailRecipient` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `ClassName` enum('UserDefinedForm_EmailRecipient') DEFAULT 'UserDefinedForm_EmailRecipient',
  `Created` datetime DEFAULT NULL,
  `LastEdited` datetime DEFAULT NULL,
  `EmailAddress` varchar(200) DEFAULT NULL,
  `EmailSubject` varchar(200) DEFAULT NULL,
  `EmailFrom` varchar(200) DEFAULT NULL,
  `EmailReplyTo` varchar(200) DEFAULT NULL,
  `EmailBody` mediumtext,
  `SendPlain` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HideFormData` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `FormID` int(11) NOT NULL DEFAULT '0',
  `SendEmailFromFieldID` int(11) NOT NULL DEFAULT '0',
  `SendEmailToFieldID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `FormID` (`FormID`),
  KEY `SendEmailFromFieldID` (`SendEmailFromFieldID`),
  KEY `SendEmailToFieldID` (`SendEmailToFieldID`),
  KEY `ClassName` (`ClassName`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `UserDefinedForm_EmailRecipient`
--

INSERT INTO `UserDefinedForm_EmailRecipient` (`ID`, `ClassName`, `Created`, `LastEdited`, `EmailAddress`, `EmailSubject`, `EmailFrom`, `EmailReplyTo`, `EmailBody`, `SendPlain`, `HideFormData`, `FormID`, `SendEmailFromFieldID`, `SendEmailToFieldID`) VALUES
(1, 'UserDefinedForm_EmailRecipient', '2013-09-08 11:58:56', '2013-09-08 11:59:08', NULL, 'Contact Form', NULL, NULL, NULL, 0, 0, 3, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `UserDefinedForm_Live`
--

CREATE TABLE `UserDefinedForm_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `SubmitButtonText` varchar(50) DEFAULT NULL,
  `OnCompleteMessage` mediumtext,
  `ShowClearButton` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DisableSaveSubmissions` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `EnableLiveValidation` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HideFieldLabels` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=22 ;

--
-- Dumping data for table `UserDefinedForm_Live`
--

INSERT INTO `UserDefinedForm_Live` (`ID`, `SubmitButtonText`, `OnCompleteMessage`, `ShowClearButton`, `DisableSaveSubmissions`, `EnableLiveValidation`, `HideFieldLabels`, `Version`) VALUES
(1, NULL, NULL, 0, 0, 0, 0, 0),
(3, NULL, NULL, 0, 0, 1, 0, 0),
(20, NULL, '<p>Thanks, we''ve received your submission.</p>', 0, 0, 0, 0, 0),
(21, NULL, '<p>Thanks, we''ve received your submission.</p>', 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `UserDefinedForm_versions`
--

CREATE TABLE `UserDefinedForm_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `SubmitButtonText` varchar(50) DEFAULT NULL,
  `OnCompleteMessage` mediumtext,
  `ShowClearButton` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `DisableSaveSubmissions` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `EnableLiveValidation` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `HideFieldLabels` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Dumping data for table `UserDefinedForm_versions`
--

INSERT INTO `UserDefinedForm_versions` (`ID`, `RecordID`, `Version`, `SubmitButtonText`, `OnCompleteMessage`, `ShowClearButton`, `DisableSaveSubmissions`, `EnableLiveValidation`, `HideFieldLabels`) VALUES
(1, 1, 8, NULL, NULL, 0, 0, 0, 0),
(2, 3, 12, NULL, NULL, 0, 0, 0, 0),
(3, 3, 13, NULL, NULL, 0, 0, 1, 0),
(4, 3, 15, NULL, NULL, 0, 0, 1, 0),
(5, 20, 1, NULL, '<p>Thanks, we''ve received your submission.</p>', 0, 0, 0, 0),
(6, 20, 2, NULL, '<p>Thanks, we''ve received your submission.</p>', 0, 0, 0, 0),
(7, 1, 12, NULL, NULL, 0, 0, 0, 0),
(8, 21, 1, NULL, '<p>Thanks, we''ve received your submission.</p>', 0, 0, 0, 0),
(9, 21, 2, NULL, '<p>Thanks, we''ve received your submission.</p>', 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `VirtualPage`
--

CREATE TABLE `VirtualPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `CopyContentFromID` (`CopyContentFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `VirtualPage_Live`
--

CREATE TABLE `VirtualPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `CopyContentFromID` (`CopyContentFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `VirtualPage_versions`
--

CREATE TABLE `VirtualPage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `VersionID` int(11) NOT NULL DEFAULT '0',
  `CopyContentFromID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `CopyContentFromID` (`CopyContentFromID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_obsolete_ContactPage`
--

CREATE TABLE `_obsolete_ContactPage` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Mailto` varchar(100) DEFAULT NULL,
  `SubmitText` mediumtext,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `_obsolete_ContactPage`
--

INSERT INTO `_obsolete_ContactPage` (`ID`, `Mailto`, `SubmitText`) VALUES
(3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `_obsolete_ContactPage_Live`
--

CREATE TABLE `_obsolete_ContactPage_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `Mailto` varchar(100) DEFAULT NULL,
  `SubmitText` mediumtext,
  PRIMARY KEY (`ID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Dumping data for table `_obsolete_ContactPage_Live`
--

INSERT INTO `_obsolete_ContactPage_Live` (`ID`, `Mailto`, `SubmitText`) VALUES
(3, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `_obsolete_ContactPage_versions`
--

CREATE TABLE `_obsolete_ContactPage_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `Mailto` varchar(100) DEFAULT NULL,
  `SubmitText` mediumtext,
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `_obsolete_ContactPage_versions`
--

INSERT INTO `_obsolete_ContactPage_versions` (`ID`, `RecordID`, `Version`, `Mailto`, `SubmitText`) VALUES
(1, 3, 6, NULL, NULL),
(2, 3, 7, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `_obsolete_Page`
--

CREATE TABLE `_obsolete_Page` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GalleryPageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GalleryPageID` (`GalleryPageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `_obsolete_Page_Live`
--

CREATE TABLE `_obsolete_Page_Live` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `GalleryPageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `GalleryPageID` (`GalleryPageID`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `_obsolete_Page_Live`
--

INSERT INTO `_obsolete_Page_Live` (`ID`, `GalleryPageID`) VALUES
(1, 0);

-- --------------------------------------------------------

--
-- Table structure for table `_obsolete_Page_versions`
--

CREATE TABLE `_obsolete_Page_versions` (
  `ID` int(11) NOT NULL AUTO_INCREMENT,
  `RecordID` int(11) NOT NULL DEFAULT '0',
  `Version` int(11) NOT NULL DEFAULT '0',
  `GalleryPageID` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  UNIQUE KEY `RecordID_Version` (`RecordID`,`Version`),
  KEY `RecordID` (`RecordID`),
  KEY `Version` (`Version`),
  KEY `GalleryPageID` (`GalleryPageID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
